def double(i: Int): Int = i * 2
double(2)
double(3)

val myFunc = (i: Int) =>{ i + 1}

def change(x: Int, callback: Int => Int):Int = {
  // despues de los dos puntos es lo que devuelve change
  return callback(x)
}

change(2,myFunc)
change(2,double)

// con closure, mas compleja: (Curring)
def dosVeces (value: Int) : (Int => Int) => Int  = {
  return (func: Int=>Int) => func(func(value))
}

// mas sencilla con dos parametros:
def dosVeces1 (value: Int, callback:Int=>Int):Int = callback(callback(value))

dosVeces1(2, x=> x+1)

dosVeces(2)(x=> x+1)
var tres = dosVeces(3)
var dos = dosVeces(2)
// dos es una variable que guarda una funcion
// el parametro de entrada es otra funcion (toma int => int) y retorna int
tres(x=>x*2)
tres(x=>x+1)
dos(x=>x*2)
dos(x=>x+1)
// dos(x=>x+1) = (x+1)+1 = 2+1+1 = 4


def ifThenElse[A](condicion: Boolean, entonces: () => A, sino: () => A ):A =
  condicion match {
    case true =>  entonces()
    case false =>  sino()
  }

ifThenElse(1>2,() =>"EsMasGrande", () =>"EsMasChico")
ifThenElse(2>1,() =>"EsMasGrande", () =>"EsMasChico")