import scala.annotation.tailrec
// Estructuras de Datos - Trabajo Practico 1

// 1) 1.

// a) sucesor :: Int -> Int
// Dado un número devuelve su sucesor

def sucesor(x: Int): Int = x + 1

sucesor(5)


// b) sumar :: Int -> Int -> Int
// Dados dos números devuelve su suma utilizando la operación +.

def sumar(x: Int, y: Int): Int = x + y

sumar(4, 3)


// 2.

// a) negar :: Bool -> Bool
// Dado un booleano, si es True devuelve False, y si es False devuelve True.

def negar(x: Boolean): Boolean = x match {
  case true => false
  case false => true
}

negar(true)
negar(false)


// b) and :: Bool -> Bool -> Bool
// Dados dos booleanos si ambos son True devuelve True, sino devuelve False.

def and(x: Boolean, y: Boolean): Boolean = (x, y) match {
  case (true, true) => true
  case (_, _) => false
}

and(false,true)
and(true,false)
and(true,true)
and(false,false)


// c) or :: Bool -> Bool -> Bool
// Dados dos booleanos si alguno de ellos es True devuelve True, sino devuelve False.

def or(x: Boolean, y: Boolean): Boolean = (x, y) match {
  case (true, _) => true
  case (_, true) => true
  case (false, false) => false
}

or(false,true)
or(true,false)
or(true,true)
or(false,false)


// f ) sumaPar :: (Int,Int) -> Int
// un par de números devuelve su suma.

def sumaPar(t:(Int,Int)):Int = t._1+t._2

sumaPar(4,7)


// 3.

// c) duplicar :: a -> (a,a)
// Dado un elemento de algún tipo devuelve un par con ese elemento en ambas componentes.
def duplicar[T](x: T) = (x,x)

duplicar("Scala")
duplicar(8)


// d ) singleton :: a -> [a]
// Dado un elemento de algún tipo devuelve una lista con este único elemento.

def singleton[T](x: T) = List(x)

singleton("Scala")


// 4.

// a) isEmpty :: [a] -> Bool
// Dada una lista de elementos, si es vacía devuelve T rue, sino devuelve F alse.

def isEmpty[T](x: List[T]): Boolean = x.length.equals(0)

isEmpty(List())
isEmpty(List(1,2,3))
isEmpty(List('h','o','l','a'))


// 2.1 - RECURSION:

// 1. sumatoria :: [Int] -> Int
// Dada una lista de enteros devuelve la suma de todos sus elementos.

def sumatoria(x: List[Int]): Int = x match {
  case Nil => 0
  case x :: xs => x + sumatoria(xs)
}

sumatoria(List(1,2,3,4))


// 3. promedio :: [Int] -> Int
// Dada una lista de enteros, devuelve un ńumero que es el promedio entre todos los elementos de la lista.

def promedio(x: List[Int]): Int = sumatoria(x) / x.length

promedio(List(2,2,3,5))


// 4. mapSucesor :: [Int] -> [Int]
// Dada una lista de enteros, devuelve la lista de los sucesores de cada entero.

def mapSucesor(x: List[Int]): List[Int] = x match {
  case Nil => Nil
  case x :: xs => x + 1 :: mapSucesor(xs)
}

mapSucesor(List(10,20,30))


// 5. mapSumaPar :: [(Int,Int)] -> [Int]
// Dada una lista de pares de enteros, devuelve una nueva lista en la que cada elemento es la
// suma de los elementos de cada par.

def mapSumaPar(x: List[(Int,Int)]): List[Int] = x match {
  case Nil => Nil
  case x :: xs => sumaPar(x) :: mapSumaPar(xs)
}

mapSumaPar(List((2,2),(3,3)))


// 7. todoVerdad :: [Bool] -> Bool
// Dada una lista de booleanos devuelve True si todos sus elementos son True.

def todoVerdad(x: List[Boolean]): Boolean = x match {
  case Nil => true
  case x :: xs => x && todoVerdad(xs)
}

todoVerdad(List(true,true,true))
todoVerdad(List(true,false,true))


// 9. pertenece :: Eq a => a -> [a] -> Bool
// Dados un elemento e y una lista xs devuelve True si existe un elemento en xs que sea igual a e.

def pertenece[T](e: T, x: List[T]): Boolean = x match {
  case Nil => false
  case x :: xs => if (e == x) true
                  else pertenece(e, xs)
}

pertenece(1, List(2,2,2,3,4,5,6))
pertenece(5, List(2,2,2,3,4,5,6))


// 13. mapLongitudes :: [[a]] -> [Int]
// Dada una lista de listas, devuelve la lista de sus longitudes. Aplique esta funcíon a la lista
// de strings ["Estructuras", "de", "datos"] y observe el resultado.

def mapLongitudes[T](x: List[List[T]]): List[Int] = x match {
  case Nil => Nil
  case x :: xs => x.length :: mapLongitudes(xs)
}

mapLongitudes(List(List("Estructuras", "de", "datos")))
mapLongitudes(List(List('h','o','l','a')))


// 15. intercalar :: a -> [a] -> [a]
// Dado un elemento e y una lista xs, ubica a e entre medio de todos los elementos de xs.

def intercalar[T](e: T, x: List[T]): List[T] = x match {
  case Nil => Nil
  case x :: xs => x :: e :: intercalar(e, xs)
}

intercalar("Arriba", List("Abajo", "Abajo", "Abajo"))
intercalar(true, List(false, false, false))


// 19. reversa :: [a] -> [a]
// Dada una lista devuelve la lista con los mismos elementos de atras para adelante.

def reversa[T](x: List[T]): List[T] = x match {
  case Nil => Nil
  case x :: xs => reversa(xs) ::: List(x)
}

reversa(List(1,2,3,4,5))



// 2.2 Recursión sobre números

// 1. factorial :: Int -> Int
//  Dado un número n se devuelve la multiplicación de este número y todos sus anteriores hasta
//  llegar a 0. Si n es 0 devuelve 1. La función es parcial si n es negativo.

def factorial(x: Int): Int = x match {
  case 0 => 1
  case 1 => 1
  case x => x * factorial (x-1)
}

factorial(0)
factorial(3)
factorial(4)


// 2. cuentaRegresiva :: Int -> [Int]
// Dado un número n devuelve una lista cuyos elementos sean los números comprendidos entre
// n y 1 (incluidos). Si el número es inferior a 1, devuelve la lista vacía.

def cuentaRegresiva(x: Int): List[Int] = x match {
  case 0 => Nil
  case x => x :: cuentaRegresiva (x-1)
}

cuentaRegresiva(0)
cuentaRegresiva(5)


// 3. contarHasta :: Int -> [Int]
// Dado un número n devuelve una lista cuyos elementos sean los números entre 1 y n (incluidos).

def desdeHasta(x: Int, y: Int): List[Int] = (x, y) match {
  case (x, y) => if (x != y) x :: desdeHasta((x + 1), y)
                  else if (x == y) x :: List()
                        else x :: desdeHasta((x + 1), y)
}

def contarHasta(x: Int): List[Int] = x match {
  case 0 => Nil
  case x => desdeHasta(1, x)
}

contarHasta(5)


// 6. takeN :: Int -> [a] -> [a]
// Dados un número n y una lista xs, devuelve una lista con los primeros n elementos de xs.
// Si la lista posee menos de n elementos, se devuelve una lista vacía.

def takeN(n: Int, x: List[Int]): List[Int] = (n,x) match {
  case (_ , Nil) => Nil
  case (0, _) => Nil
  case (n, x :: xs) => if (xs.size < n) Nil
                        else x :: takeN((n - 1), xs)
}

takeN(4, List(9,8,7,6,5,4,3,2,1))
takeN(3, List(1,2,3,4))
takeN(4, List(1,2,3,4))
takeN(5, List(1,2,3,4,5,6,7))
takeN(8, List(1,2,3,4))
takeN(5, List(1,2,3,4))