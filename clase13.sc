// 1) Lista de números de 1 a 10

val oneToTen = (1 to 10).toList


// 2) Función que pasa un número de 1 a 10 a letras

def numeroALetra (numero: Int): String = numero match {
  case 1 => "uno"
  case 2 => "dos"
  case 3 => "tres"
  case 4 => "cuatro"
  case 5 => "cinco"
  case 6 => "seis"
  case 7 => "siete"
  case 8 => "ocho"
  case 9 => "nueve"
  case 10 => "diez"
}

numeroALetra(5)


// 3) Con una sola funcion pasar la lista de 10 elementos a letras

def listaNumerosALetra(numeros: List[Int]): List[String] = numeros match {
  case Nil => Nil
  case x :: xs => numeroALetra(x) :: listaNumerosALetra(xs)
}

def listaNumerosALetra2(numeros: List[Int]): List[String] = numeros.map(x=>numeroALetra(x))

listaNumerosALetra(oneToTen)
listaNumerosALetra2(oneToTen)


// 4) Quedarse con los elementos de mas de 4 letras

def elementosDeMasDeCuatroLetras(numeros: List[Int]): List[String] =
  listaNumerosALetra(numeros) filter(numero => numero.length > 4)

elementosDeMasDeCuatroLetras(oneToTen)


// 5) Contar cuantas letras hay en total en la lista

def cantidadDeLetras(numeros: List[Int]): Int =
  listaNumerosALetra(numeros).foldRight(0)((x, y) => y + x.length)

def cantidadDeLetras2(numeros: List[Int]): Int =
  listaNumerosALetra(numeros).map(x => x.length).reduce((a, b) => a + b)

cantidadDeLetras(oneToTen)
cantidadDeLetras2(oneToTen)


// 6) Dada una lista xs de enteros devuelva una tupla de listas, donde la primera componente contiene todos
// aquellos numeros positivos de xs y la segunda todos aquellos numeros negativos de xs.

def particionPorSigno(numeros: List[Int]): (List[Int], List[Int]) = {
  val negativos = numeros filter(n => n < 0)
  val positivos = numeros filter(n => n > 0)
  (negativos, positivos)
}

def particionPorSigno2(numeros: List[Int]): (List[Int], List[Int]) = {
  numeros.foldRight((List[Int](), List[Int]()))((a, b) => if(a >= 0) {(b._1, a::b._2)}
                                                          else {(a::b._1, b._2)})
}

particionPorSigno(List(-4,-2,6,-5, 8))
particionPorSigno2(List(-4,-2,6,-5, 8))


// 7) Dada una lista devuelve cada sublista resultante de aplicar tail en cada paso. Ejemplo:
//  subtails "abc" == ["abc", "bc", "c",""]

def subtails[Type](num: List[Type]): List[List[Type]] = num match {
  case Nil => List()
  case x :: xs => (x :: xs) :: subtails(xs)
}

subtails(List('a','b','c'))
