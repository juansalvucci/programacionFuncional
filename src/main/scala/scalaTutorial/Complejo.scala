package scalaTutorial
/*
class Complejo(real: Double, imaginaria: Double) {
  def re() = real
  def im() = imaginaria
}
*/

class Complejo(real: Double, imaginaria: Double) {
  def re = real
  def im = imaginaria
  override def toString() =
    "" + re + (if (im < 0) "" else "+") + im + "i"
}