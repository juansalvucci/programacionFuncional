package scalaTutorial

object NumerosComplejos {
  def main(args: Array[String]) {
    val c = new Complejo(1.2, 3.4)
    println("Parte imaginaria: " + c.im())
  }
}
