package scalaTutorial

class Referencia[T] {
  private var contenido: T = _
  def set(valor: T) { contenido = valor }
  def get: T = contenido
}