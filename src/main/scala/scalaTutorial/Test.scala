package scalaTutorial

object Test {

  // ARBOL:
  type Entorno = String => Int

  def eval(a: Arbol, ent: Entorno): Int = a match {
    case Sum(i, d) => eval(i, ent) + eval(d, ent)
    case Var(n)    => ent(n)
    case Const(v)  => v
  }

  def derivada(a: Arbol, v: String): Arbol = a match {
    case Sum(l, r) => Sum(derivada(l, v), derivada(r, v))
    case Var(n) if (v == n) => Const(1)
    case _ => Const(0)
  }


  def main(args: Array[String]) {
    val exp: Arbol = Sum(Sum(Var("x"),Var("x")),Sum(Const(7),Var("y")))
    val ent: Entorno = { case "x" => 5 case "y" => 7 }
    println("Expresión: " + exp)
    println("Evaluación con x=5, y=7: " + eval(exp, ent))
    println("Derivada con respecto a x:\n " + derivada(exp, "x"))
    println("Derivada con respecto a y:\n " + derivada(exp, "y"))


    // FECHAS:
    var f1 = new Fecha(14, 9, 1983)
    var f2 = new Fecha(14, 9, 2021)

    f1 >= f2
    println(f1 >= f2)

    
    // REFERENCIAS:
    val ref = new Referencia[Int]
    ref.set(13)
    println("La referencia tiene la mitad de " + (ref.get * 2))


    val ref2 = new Referencia[String]
    ref2.set("Esto es una prueba de tipos genericos")
    println(ref2.get)
  }
}
