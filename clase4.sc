// 3 formas de escribir lo mismo:

val salaries = Seq(20000, 70000, 40000)
val doubleSalary = (x: Int) => x * 2
val newSalaries = salaries.map(doubleSalary) // List(40000, 140000, 80000)


val salaries = Seq(20000, 70000, 40000)
val newSalaries = salaries.map(x => x * 2) // List(40000, 140000, 80000)


val salaries = Seq(20000, 70000, 40000)
val newSalaries = salaries.map(_ * 2)


// ---------------------------------------------------------------------------------------
// Funcion que retorna funciones:

def urlBuilder(ssl: Boolean, domainName: String)
:
(String, String) => String   // Retorna
=
{
  val schema = if (ssl) "https://" else "http://"
  (endpoint: String, query: String) => s"$schema$domainName/$endpoint?$query"
}

val domainName = "www.example.com"
def getURL = urlBuilder(ssl=true, domainName)
// https://www.example.com

val endpoint = "users"
val query = "id=1"
val url = getURL(endpoint, query) // "https://www.example.com/users?id=1": String


// ---------------------------------------------------------------------------------------
// Implementacion de while:

def whileLoop(condition: => Boolean)(body: => Unit)
:
Unit
=
  if (condition) {
    body
    whileLoop(condition)(body)
  }

var i = 2

whileLoop (i > 0) {
  println(i)
  i -= 1
}  // prints 2 1


// ---------------------------------------------------------------------------------------
// Lazy evaluation:
lazy val param= 1/0
def demo(x:Int):Int ={return 2}
//println(demo(param))



// ---------------------------------------------------------------------------------------
// Pattern matching:
def matchTest(x: Int): String = x match {
  case 1 => "one"
  case 2 => "two"
  case _ => "other"
}
matchTest(3)  // prints other
matchTest(1)  // prints one



// ---------------------------------------------------------------------------------------
// Matching on case classes:
abstract class Notification

case class Email(sender: String, title: String, body: String) extends Notification

case class SMS(caller: String, message: String) extends Notification

case class VoiceRecording(contactName: String, link: String) extends Notification


def showNotification(notification: Notification)
:
String // (Retorna)
=
{
  notification match {
    case Email(sender, title, _) =>
      s"You got an email from $sender with title: $title"
    case SMS(number, message) =>
      s"You got an SMS from $number! Message: $message"
    case VoiceRecording(name, link) =>
      s"You received a Voice Recording from $name! Click the link to hear it: $link"
  }
}

val someSms = SMS("12345", "Are you there?")
val someVoiceRecording = VoiceRecording("Tom", "voicerecording.org/id/123")

println(showNotification(someSms))  // prints You got an SMS from 12345! Message: Are you there?

println(showNotification(someVoiceRecording))  // prints You received a Voice Recording from Tom! Click the link to hear it: voicerecording.org/id/123