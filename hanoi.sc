
type Tablero = List[List[Int]]

type pasos = (Int, Int, Int)

def inicializarTablero(cantidadFichas: Int): Tablero =  List((1 to cantidadFichas).toList, List(), List())

def evaluarTorre(torre: Int, tab: Tablero, origen: Int, destino: Int): List[Int] = {
  if (torre==origen) {
    tab.apply(torre).tail
  } else if (torre.equals(destino)) {
    tab.apply(origen).head::tab.apply(torre)
  } else {
    tab.apply(torre)
  }
}

def hanoi(tab: Tablero, fichasAMover: Int, origen: Int, destino: Int, auxiliar: Int): (Tablero, List[pasos]) = fichasAMover match {
  case 1 => (List(evaluarTorre(0, tab, origen, destino), evaluarTorre(1, tab, origen, destino), evaluarTorre(2, tab, origen, destino)),
            List((tab.apply(origen).head, origen, destino)))
  case _ => val R1 = hanoi(tab, fichasAMover - 1, origen, auxiliar, destino)
            val R2 = hanoi(R1._1,  1, origen, destino, auxiliar)
            val R3 = hanoi(R2._1,  fichasAMover - 1, auxiliar, destino, origen)
            (R3._1, R1._2 ::: R2._2 ::: R3._2)
}

hanoi(inicializarTablero(3), 1, 0,2, 1)
hanoi(inicializarTablero(3), 2, 0,2, 1)
val x = hanoi(inicializarTablero(4), 4, 0,2, 1)

for (paso <- x._2) println("Mover ficha " + paso._1 + " Desde torre " + paso._2 + " Hasta torre " + paso._3)

