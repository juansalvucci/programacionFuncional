def calcBinario(numero: List[Int]): List[List[Int]] = {
  if (numero.length == 3) {
    if (numero.count(e => e == 1) >= 2) {
      return List(numero)
    }
    List()
  }
  else {
    val R0 = calcBinario(numero ++ List(0))
    val R1 = calcBinario(numero ++ List(1))
    R0 ++ R1
  }
}

def calcularBinario(): List[List[Int]] = calcBinario(List())

calcularBinario()



