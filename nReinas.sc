
def estaBienPosicionado(numero: Int, tablero: List[Int], contador: Int): Boolean = tablero match {
  case Nil => true
  case x::xs => numero + contador != x && numero - contador != x && estaBienPosicionado(numero, xs, contador + 1)
}

def esCorrecta (resultado: List[Int]): Boolean = resultado match {
  case Nil => true
  case x::xs => estaBienPosicionado (x, xs, 1) && esCorrecta(xs)
}

def nReinasSolucion(tablero: List[Int], n: Int): List[List[Int]] = {
  if (tablero.length == n) {
    if (esCorrecta(tablero)) {
      List(tablero)
    } else {
      List()
    }
  }
  else {
    var resultados: List[List[Int]] = List()
    for (i <- 1 to n) {
      if (!tablero.contains(i)) {
        resultados = resultados ++ nReinasSolucion(tablero ++ List(i), n)
      }
    }
    resultados
  }
}

def nReinas(cantidadDeReinas: Int): List[List[Int]] = nReinasSolucion(List(), cantidadDeReinas)

nReinas(8)