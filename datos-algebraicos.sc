// 1.

sealed trait Dir
  case object Norte extends Dir
  case object Sur extends Dir
  case object Este extends Dir
  case object Oeste extends Dir

def opuesto(x: Dir): Dir = x match {
  case Norte => Sur
  case Sur => Norte
  case Este => Oeste
  case Oeste => Este
}

opuesto(Norte)
opuesto(Este)

def siguiente(x: Dir): Dir = x match {
  case Norte => Este
  case Este => Sur
  case Sur => Oeste
  case Oeste => Norte
}

siguiente(Norte)
siguiente(Sur)


// 2 Polinomio:

abstract class Polinomio
  case class Const(n: Int) extends Polinomio
  case class Var() extends Polinomio
  case class Add(p1: Polinomio, p2: Polinomio) extends Polinomio
  case class Mul(p1: Polinomio, p2: Polinomio) extends Polinomio


def eval(p: Polinomio, x: Int): Int = p match {
  case Const(n) => n
  case Var() => x
  case Add(p1, p2) => eval(p1, x) + eval(p2, x)
  case Mul(p1, p2) => eval(p1, x) * eval(p2, x)
}

eval(Const(5), 2)
eval(Add(Const(6), Var()), 4)
eval(Add(Const(3), Mul(Var(), Var())), 4)


def mEscalar(p: Polinomio, x: Int): Polinomio = p match {
  case Const(n) => Const(n*x)
  case Var() => Mul(Const(x), Var())
  case Add(p1, p2) => Add(mEscalar(p1, x), mEscalar(p2, x))
  case Mul(p1, p2) => Mul(mEscalar(p1, x), mEscalar(p2, x))
}

mEscalar(Const(5), 2)
mEscalar(Add(Const(6), Var()), 4)
mEscalar(Add(Const(3), Mul(Var(), Var())), 4)


def sOptimize(p: Polinomio): Polinomio = p match {
  case Const(n) => Const(n)
  case Var() => Var()
  case Add(Const(n), Const(m)) => Const(n + m)
  case Mul(Const(n), Const(m)) => Const(n * m)
  case Add(p1, p2) => Add(sOptimize(p1), sOptimize(p2))
  case Mul(p1, p2) => Mul(sOptimize(p1), sOptimize(p2))
}

sOptimize(Add(Const(6), Const(6)))
sOptimize(Add(Const(3), Mul(Const(6), Const(6))))