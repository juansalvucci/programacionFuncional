// 1
def quitaDuplicados(x: List[String]): List[String] = x match {
  case Nil => Nil
  case x :: xs => if (xs.contains(x)) quitaDuplicados(xs)
                  else x :: quitaDuplicados(xs)
}

quitaDuplicados(List("Hola", "Hola", "Chau"))


// 2
def merge(x: List[Int], y: List[Int]): List[Int] = (x, y) match {
  case (Nil, Nil) => Nil
  case (Nil, x :: xs) => x :: xs
  case (x :: xs, Nil) => x :: xs
  case (x :: xs, y :: ys) => if (x == y) x :: merge(xs, ys)
                              else if (x < y) x :: merge(xs, y :: ys)
                                    else y :: merge(x :: xs, ys)
}

merge(List(1, 2, 8, 12, 23, 56), List(3, 8, 10, 22, 48))


// 3
def miMap(f: (Int => Int), x: List[Int]): List[Int] = x match {
  case Nil => Nil
  case x :: xs => f(x) :: miMap(f, xs)
}

def sucesor(x: Int): Int = x + 1

miMap(sucesor, List(1, 2, 3))


// 4
def numTests(f: List[Int => Boolean], n: Int): Int = f match {
  case Nil => 0
  case f :: fs => if (f(n)) 1 + numTests(fs, n)
                  else numTests(fs, n)
}

def mayorQue8(x: Int): Boolean = x > 8
def par(x: Int): Boolean = x % 2 == 0
def impar(x: Int): Boolean = x % 2 != 0

numTests(List(mayorQue8, par, impar), 12)
numTests(List(mayorQue8, par, impar), 3)



// Funciones de Alto Orden de Listas:

// 1.
def sumatoria(l: List[Int]): Int = l.reduce(_+_)

sumatoria(List(1,2,3))


// 3
def promedio(x: List[Int]): Int = x.sum / x.length

promedio(List(2,2,3,5))


// 4
def mapSucesor(x: List[Int]): List[Int] = x.map(_ + 1)

mapSucesor(List(10,20,30))


// 5
def mapSumaPar(x: List[(Int,Int)]): List[Int] = x.map(p => p._1+p._2)

mapSumaPar(List((2,2),(3,3)))


// 7
def todoVerdad(x: List[Boolean]): Boolean = x.fold(true) ((e1, e2) => e1 && e2)

todoVerdad(List(true,true,true))
todoVerdad(List(true,false,true))


// 9
def pertenece[T](e: T, x: List[T]): Boolean = x.contains(e)

pertenece(1, List(2,2,2,3,4,5,6))
pertenece(5, List(2,2,2,3,4,5,6))


// 13
def mapLongitudes[T](x: List[List[T]]): List[Int] = x.map(l => l.length)

mapLongitudes(List(List("Estructuras", "de", "datos")))
mapLongitudes(List(List('h','o','l','a')))