/*
Parcial 5 de Julio 2021
Primera Fecha
Programación Funcional

Se hace en Scala en la computadora. Pueden usar los recursos que ya tienen, internet. Menos un compañero, lo que quieran.
Se entrega creando un merge request en git con solo el archivo del examen y poniendo al profesor como revisor.
Cada ejercicio tienen que hacerlo en dos versiones, recursión con pattern matching y funciones de lista de alto orden.
*/


// 1) filtrarMenoresA :: Int -> [Int] -> [Int]
// Dados un número n y una lista xs, devuelve todos los elementos de xs que son menores a n.

def filtrarMenoresA(n: Int, lista: List[Int]): List[Int] = (n, lista) match {
  case (_, Nil) => Nil
  case (0, _) => Nil
  case (n, x :: xs) => if (x < n) x :: List() ::: filtrarMenoresA(n, xs)
  else filtrarMenoresA(n, xs)
}

def filtrarMenoresA2(n: Int, lista: List[Int]): List[Int] = lista.filter(x => x < n)

val lista = List(1, 2, 5, 6, 8, 7, 4, 65 ,98 ,5 ,2)

filtrarMenoresA(7, lista)
filtrarMenoresA2(7, lista)


// 2) aplanar :: [ [a] ] -> [a]
// Dada una lista de listas, devuelve una única lista con todos sus elementos.

def aplanar[Type](x: List[List[Type]]): List[Type] = x match {
  case Nil => Nil
  case x :: xs => x ::: aplanar(xs)
}

def aplanar2[Type](x: List[List[Type]]): List[Type] = x.reduce((a, b) => a.concat(b))

val lista5 = List(1, 2, 3, 4)
val lista6 = List(5, 6, 7, 8)
val lista7 = List(lista5, lista6)
val lista8 = List(List('H','o','l','a'), List('M','u','n','d','o'))

aplanar(lista7)
aplanar(lista8)

aplanar2(lista7)
aplanar2(lista8)


// 3) sumar :: [Int] -> Int
// suma una lista enteros.

def sumar(x: List[Int]): Int = x match {
  case Nil => 0
  case x :: xs => x + sumar(xs)
}

def sumar2(x: List[Int]): Int = x.fold(0)((m, n) => m + n)

sumar(List(1, 2, 3, 4))
sumar2(List(1, 2, 3, 4))


// 4) sumarListas :: [ [Int] ] -> Int
// Tomar una Lista de Listas de enteros y sumar todos los datos.

def sumarListas(x: List[List[Int]]): Int = x match {
  case Nil => 0
  case x :: xs => sumar(x) + sumarListas(xs)
}

def sumarListas2(x: List[List[Int]]): Int = x.map(l =>l.sum).reduce((a, b) => a + b)

val lista2 = List(List(1, 2, 3, 4))
val lista3 = List(1, 2, 3, 4)
val lista4 = List(lista3, lista3)

sumarListas(lista2)
sumarListas(lista4)

sumarListas2(lista2)
sumarListas2(lista4)